# Modélisation numérique des solides et structures (CIVIL-321)

[Fiche de cours](https://edu.epfl.ch/coursebook/en/numerical-modelling-of-solids-and-structures-CIVIL-321)

## Organisation semestre printemps

### Équipe enseignante

*Cours:*

- Pr. J.-F. Molinari, directeur LSMS (http://lsms.epfl.ch), jean-francois.molinari@epfl.ch
- Dr. G. Anciaux, guillaume.anciaux@epfl.ch

*Exercices:*

- Prof. J.-F. Molinari
- Dr. G. Anciaux
- Shad Durussel
- Roxane Ferry
- Thibault Ghesquières

### Programme par semaine

Il est [ici](https://gitlab.epfl.ch/anciaux/mnss-notebooks/-/blob/main/program.pdf)

### Jupyter Notebooks

[noto.epfl.ch](https://noto.epfl.ch/hub/user-redirect/git-pull?repo=https://gitlab.epfl.ch/anciaux/mnss-notebooks&urlpath=lab%2Ftree%2Fmnss-notebooks)

### Git repository

[gitlab.epfl.ch](https://gitlab.epfl.ch/anciaux/mnss-notebooks)

### Communication

- Site Moodle pour communication (sujets, corrections, devoirs).
- Les questions sont encouragées sur le [forum]()
- Les vidéos du cours sont disponibles sur ce [lien](https://mediaspace.epfl.ch/channel/CIVIL-321+Mod%C3%A9lisation+num%C3%A9rique+des+solides+et+structures/)

### Horaires

- Cours les  jeudi: 8h15 à 10h.
- Exercices les  jeudi: 10h15 à 12h.

### Références du cours

- A first course in the Finite Element Method, Daryl L. Logan, Cengage Learning (référence principale). [\[link\]](https://istasazeh-co.com/wp-content/uploads/2022/02/Daryl-L-Logan-A-First-Course-in_the-Finite-Element-Method-CL-Engineering.pdf)
- The Finite Element Method, Linear Static and Dynamic Finite Element Analysis, T.J.T. Hughes, Dover Publications.

# Technical information for exercices

In order to run these tutorials, you have to install the dependencies:

```sh
apt install libgl1-mesa-dev libglu1-mesa-dev ffmpeg
pip install --user -r requirements.txt
```




